<?php
/* 
    Dit is een file uit een groter project (webshop)
    Dit is de AdminController, die de admin omgeving aanstuurt,
    gegevens valideert en wijzigt in de bijbehorende DataBases
*/

namespace App\Http\Controllers;

//Including all Models and Controllers needed
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Product;
use App\Order;
use App\Shipping;
use App\Http\Controllers\ProductController;
use Cache;

class AdminController extends Controller {

    //Constructor, using middleware admin
    public function __construct() {
        $this->middleware('admin');
    }

    /*
    User Management
    */

    //Return all users to the view
    public function users() {
        return view('admin-user', ['users' => User::all()]);
    }

    //Returns all orders from the specified user
    public function userDetail($id) {
        $orders = User::find($id)->orders;
        $orders->transform(function($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('admin-user-detail', ['user' => User::find($id), 'orders' => $orders]);
    }

    //Delete a user and return to user-overview
    public function userDelete($id) {
        User::destroy($id);
        return redirect()->route('admin.users', ['users' => User::all()]);
    }

     //Add a user
     public function addUser() {
        //Validating and checking input (email should be unique)
        $this->validate(request(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        //create a new User
        $user = new User();
        $user->name = request('name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        //Save new User to DB 
        $user->save();
        return redirect()->back();
    }

    //Edit user shipping details
    public function editShipping($id) {
        //Find specified shipping details from User
        $shipping = Shipping::where('user_id', $id)->first();
        //If no shipping details, create new Shipping
        if (empty($shipping)) {
            $shipping = new Shipping();
        } 
        //Validate input fields
        $this->validate(request(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'adress' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);

        //Update or incase of new Shipping details set the variables
        $shipping->user_id = $id;
        $shipping->first_name = request('first_name');
        $shipping->last_name = request('last_name');
        $shipping->adress = request('adress');
        $shipping->postal_code = request('postal_code');
        $shipping->city = request('city');
        $shipping->country = request('country');
        //Save the data to DB
        $shipping->save();
        return back();
    }

    /*
    Product Management
    */

    //Return all products to the view
    public function products() {
        return view('admin-products', ['products' => Product::all()]);
    }

    //Delete Product
    public function productDelete($id) {
        Product::destroy($id);
        return redirect()->route('admin.products');
    }

    //Product detail
    public function productDetail($id) {
        $product = Product::find($id);
        return view('admin-product-detail', ['product' => $product]);
    }

    //Add a product to the DataBase
    public function addProduct() {
        //Validating all input fields
        $this->validate(request(), [
            'imagePath' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);
        //Create new Product
        $product = new Product();
        $product->imagePath = request('imagePath');
        $product->title = request('name');
        $product->description = request('description');
        $product->price = request('price');
        //Save new Product to DB
        $product->save();
        return redirect()->route('admin.products');
    }

    //Edit Product
    public function productEdit($id) {
        //Find speficied product
        $product = Product::find($id);
        //Validating input
        $this->validate(request(), [
            'name' => ['required', 'string', 'max:255'],
            'imagePath' => ['required'],
            'description' => ['required'],
            'price' => ['required']
        ]);
        //Updating all verified variables
        $product->title = request('name');
        $product->imagePath = request('imagePath');
        $product->description = request('description');
        $product->price = request('price');
        //Save Product to DB (updating)
        $product->save();  
        return redirect()->back();
    }
   
    /*
    Order Management
    */

    //Get all orders (serialized an object cart in DB and unserialized now so it can be accessed)
    public function getOrders() {
        $orders = Order::all();
        $orders->transform(function($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('admin-orders', ['orders' => $orders]);
    }

    //Get all orders from set timeframe
    public function getTimedOrders($time) {
        //Create emtpy array
        $dailyOrders = [];
        //Fill array with orders per day from the set timeframe (using this to process data later in JS on front end)
        for ($x = 1; $x < $time +1; $x++) {
            $checkTime = date('Y-m-d H:m:s',time() - (60 * 60 * 24 * $x));
            $orders = AdminController::timeOrder($checkTime);
            array_push($dailyOrders, $orders);
        }

        //Get all orders from set timeframe
        $checkTime = date('Y-m-d H:m:s',time() - (60 * 60 * 24 * $time));
        $orders = AdminController::timeOrder($checkTime);
        $total = 0;
        //Calculate total price 
        foreach ($orders as $order) {
            $total += $order->cart->totalPrice;
        }
        return view('admin-finance', ['orders' => $orders, 'total' => $total, 'dailyOrders' => $dailyOrders, 'time' => $time]);
    }

    //Refactored function to retrieve orders (and unserialize object cart)
    public function timeOrder($checkTime) {
        $orders = Order::whereDate('created_at', '>', $checkTime)->orderBy('created_at', 'asc')->get();
            $orders->transform(function($order, $key) {
                $order->cart = unserialize($order->cart);
            return $order;
        });
        return $orders;
    }

    //Returns specified order details
    public function orderDetail($id) {
        $orders = Order::where('payment_id',$id)->first();
        $c = AdminController::getCurrency(User::find($orders->user_id)->currency);
        return view('admin-order-detail', ['order' => $orders, 'c_factor' => $c]);
    }

    /* 
    Accessing the Cache memory where exchange rates are stored (the exchange rates in chache
    are auto-destroyed after 60 minutes and updated if someone accesses the site (by means of an API))
    */
    public function getCurrency($c) {
        switch($c) {
            case '€':
            $currency_factor = (double) Cache::get('EUR');
            break;
            case '$':
            $currency_factor = (double) Cache::get('USD');
            break;
            case '£':
            $currency_factor = (double) Cache::get('GBP');
            break;
        }
        return $currency_factor;
    }

}

